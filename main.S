.syntax unified
.code 16 //; Thumb aka 16-bit instruction set.

.equ GPIOA_MODER, 0x48000000 //; GPIOA mode reg address.
.equ GPIOA_ODR, 0x48000014 //; GPIOA output reg address.
.equ RCC_AHBENR, 0x40021014 //; Clock settings address.
.equ STACKINIT, 0x20001000 //; Stack initial value ( 4K ).

.equ DELAY, 3200000 //; Time of cycles to wait till next output change.

.equ LED_MASK, 0x0000000f //; Mask for LED 7-segment display.

//; Placed in boot memory.
.section .text
	
vec:
	.word STACKINIT
	//; Manually aligning vectors for Thumb.
	.word _start + 1
	.word _nmi_hand + 1
	.word _hard_fault + 1
	.word _mem_fault + 1
	.word _bus_fault + 1
	.word _usage_fault + 1
	
_start:
	//; Set stack value.
	ldr r12, =STACKINIT
	//; Enable GPIOA clock, preserving initial settings.
	ldr r7, =RCC_AHBENR
	ldr r1, [r7]
	ldr r0, =0x00020000
	orr r1, r0
	str r1, [r7]
	
	//; Enable output of pin PA2.
	ldr r7, =GPIOA_MODER
	ldr r1, [r7]
	ldr r0, =0x00005554
	orr r1, r0
	str r1, [r7]
	
	//; Set variables for output reg of GPIOA.
	ldr r4, =0
	
	//; Load address of GPIOA output reg.
	ldr r7, =GPIOA_ODR
	
_loop:
	//; Get digit.
	mov r0, r4
	bl _get_digit
	//; Store to GPIO_ODR.
	str r0, [r7]
	adds r4, 1
	//; Load delay value.
	ldr r6, =DELAY
	
_delay:
	//; Decreament by one.
	subs r6, 1
	//; Branch if zero.
	bne _delay
	//; Else XOR the output register.
	eor r4, r5
	//; If XORed then return to _loop.
	b _loop
	
_get_digit:
	//; Preserving values.
	push {r4, r5, r6, r7, lr}
	//; Load in LED mask.
	ldr r4, =LED_MASK
	and r4, r4, r0
	//; Add address to ROM to get digit.
	ldr r5, =digit_rom
	add r5, r5, r4
	//; Return the pattern.
	ldr r0, [r5]
	//; Getting back those and go back to LR.
	pop {r4, r5, r6, r7, pc}
	
//; Everything else branch here.
_nmi_hand:
_hard_fault:
_mem_fault:
_bus_fault:
_usage_fault:
//; Infinite loop.
_end:
	b _end

//; Digit ROM here.
.section .rom
digit_rom:
	//; Chars 0..7
	.byte 0xfc
	.byte 0x48
	.byte 0xba
	.byte 0xda
	.byte 0x4e
	.byte 0xd6
	.byte 0xf6
	.byte 0x58
	//; Chars 8..15
	.byte 0xfe
	.byte 0xde
	.byte 0x7e
	.byte 0xe6
	.byte 0xa2
	.byte 0xea
	.byte 0xb6
	.byte 0x36
	
