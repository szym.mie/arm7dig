TARGET = main

LD_FILE = SECTIONS.ld
MCU = cortex-m4

AS = arm-none-eabi-as
LD = arm-none-eabi-ld
OC = arm-none-eabi-objcopy
SZ = arm-none-eabi-size

LIST_FILE = main.lst

ASF += -mcpu=$(MCU)
ASF += -mthumb
ASF += -mapcs-32
ASF += -gstabs
ASF += -ahls=$(LIST_FILE)

LDF += -T $(LD_FILE)
LDF += -nostdlib
LDF += -nostartfiles

OCF += -S
OCF += -O binary

ASS += main.asm

OBJS += $(ASS:.asm=.o)

.PHONY: all
all: $(TARGET).bin

%.o: %.asm
	$(AS) $(ASF) -o $@ $<
	
$(TARGET).elf: $(OBJS)
	$(LD) $(LDF) -o $@ $^
	
$(TARGET).bin: $(TARGET).elf
	$(OC) $(OCF) $< $@
	$(SZ) $<
	
.PHONY: clean
clean:
	rm -f $(OBJS)
	rm -f $(LIST_FILE)
	rm -f $(TARGET).elf
	rm -f $(TARGET).bin
