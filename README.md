# ARM assembly 7-digit display example

## Requirments:
- designed for Cortex-M4.
- compiled using arm-none-eabi.
- tested on STM303K8T6.

## Specifics

This example uses PortA GPIO - to be specific pins A1-A7. I haven't got access to common-anode displays, so I have only used common-cathode ones - still it should work since it takes advantage of *push-pull* output.

## Wiring diagram

    --A--
    F   B
    --G--
    E   C
    --D--

    G F _ A B
    ---------
    |       |
    |       |
    |       |
    ---------
    E D _ C .

    _ ground, COM

(sorry for this crude schematic)

| segment | STM32 pin |
|:-------:|:---------:|
| A       | A3        |
| B       | A2        |
| C       | A5        |
| D       | A6        |
| E       | A4        |
| F       | A7        |
| G       | A1        |

As you may see it is totally out of order - you might want to clean it up, since all the symbols reside in *ROM* section.


